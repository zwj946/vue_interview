import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import Home from '@/views/home/home'
import Mock from '@/views/mock/mock'
import Teclolege from '@/views/teclolege/teclolege'
import Profile from '@/views/profile/profile'

const router = new Router({
    mode: 'history',
    routes: [{
        path: '/',
        redirect: '/home',
    }, {
        path: '/home',
        component: Home,
    }, {
        path: '/mock',
        component: Mock,
    }, {
        path: '/teclolege',
        component: Teclolege,
    }, {
        path: '/profile',
        component: Profile,
    }]
})

const originalPush = router.push
router.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

export default router