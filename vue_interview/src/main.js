import Vue from 'vue'
import App from './App.vue'

import Less from 'less'
Vue.use(Less)

//使用rem布局
import 'amfe-flexible'

import '@/assets/css/normalize.css'
import '@/assets/css/common.css'

import router from '@/router/router'


Vue.config.productionTip = false

new Vue({
    render: h => h(App),
    router,
}).$mount('#app')